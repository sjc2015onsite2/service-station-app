/**
 * 
 * Пакет содержит имплементации сервисного слоя.
 * 
 * @author Alexey Suslov
 *
 */
package com.expositds.sjc.servicestation.app.service;